============
Installation
============

At the command line::

    $ easy_install google_web_scraper

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv google_web_scraper
    $ pip install google_web_scraper
