#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_main
----------------------------------

Tests for `google_web_scraper` module.
"""

import unittest

from google_web_scraper import main


class TestGoogle_web_scraper(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_main(self):
        pass


if __name__ == '__main__':
    import sys
    sys.exit(unittest.main())
