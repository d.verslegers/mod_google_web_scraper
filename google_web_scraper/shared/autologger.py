import functools
import logging


def arg2dict(args):
    result = {}
    for arg in args:
        result[arg] = obj2dict(arg)

    return result


def obj2dict(obj):
    if not hasattr(obj, "__dict__"):
        return obj
    result = {}
    for key, val in obj.__dict__.items():
        if key.startswith("_"):
            continue
        element = []
        if isinstance(val, list):
            for item in val:
                element.append(obj2dict(item))
        else:
            element = obj2dict(val)
        result[key] = element
    return result


class AutoLogger(object):
    """Logging decorator that allows you to log with a specific logger."""

    # Customize these messages
    ENTRY_MESSAGE = 'Entering {0} with params {1}'
    EXIT_MESSAGE = 'Exiting {0} with result {1}'
    EXCEPTION_MESSAGE = 'Crashed in module {0} with error {1}'

    def __init__(self, logger=None):
        self.logger = logger

    def __call__(self, function):
        """Returns a wrapper that wraps func. The wrapper will log the entry and exit points of the function
        with logging.INFO level."""

        # set logger if it was not set earlier
        if not self.logger:
            logging.basicConfig()
            self.logger = logging.getLogger(function.__module__)
            # create the logging file handler
            fh = logging.FileHandler(r"/tmp/teobot.log")

            fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
            formatter = logging.Formatter(fmt)
            fh.setFormatter(formatter)

            self.logger.addHandler(fh)

        @functools.wraps(function)
        def wrapper(*args, **kwargs):
            try:
                self.logger.info(self.ENTRY_MESSAGE.format(function.__qualname__, arg2dict(args)))  # logging level .info(). Set to .debug() if you want to
                f_result = function(*args, **kwargs)
                self.logger.info(self.EXIT_MESSAGE.format(function.__qualname__, obj2dict(f_result)))   # logging level .info(). Set to .debug() if you want to
                return f_result
            except Exception as e:
                self.logger.error(self.EXCEPTION_MESSAGE.format(function.__qualname__, str(e.args[0])))
                raise
        return wrapper
