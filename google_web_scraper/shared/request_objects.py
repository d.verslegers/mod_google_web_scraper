class InvalidRequestObject(object):
    """
    Base class for invalid request objects
    """

    def __init__(self):
        self.errors = []

    def add_error(self, parameter, message):
        """
        Function to add errors to the invalid request object.

        :param parameter: The request object parameter which is invalid
        :param message: The error message
        """
        self.errors.append({'parameter': parameter, 'message': message})

    def has_errors(self):
        """
        Function to list the number of errors linked to this invalid request object.

        :return: Number of invalid requests
        """
        return len(self.errors) > 0

    def __nonzero__(self):
        """
        Function to ensure that validation of this object results in False

        :return: False
        """
        return False

    __bool__ = __nonzero__


class ValidRequestObject(object):
    """
    Base class for valid request objects
    """

    @classmethod
    def from_dict(cls, adict):
        raise NotImplementedError

    def __nonzero__(self):
        """
        Function to ensure that validation of this object results in True

        :return: True
        """
        return True

    __bool__ = __nonzero__
