from google_web_scraper.shared import response_objects as res


class UseCase(object):
    """
    Base class for use case objects
    """

    def execute(self, request_object):
        """
        The execute function is to be called whenever a use case is to be executed. The execute function accepts a valid
        request object and will try to process the request by calling the process_request function. The use case will
        return a response failure if an invalid request object is provided.

        :param request_object: request object to be treated by the use case
        :return: response failure object or response success object
        """
        if not request_object:
            return res.ResponseFailure.build_from_invalid_request_object(request_object)
        try:
            return self.process_request(request_object)
        except Exception as exc:
            return res.ResponseFailure.build_system_error(
                "{}: {}".format(exc.__class__.__name__, "{}".format(exc)))

    def process_request(self, request_object):
        """
        The process request method is to be overwritten by each use case to implement the specific actions to be
        performed in the context of the use case.
        """
        raise NotImplementedError(
            "process_request() not implemented by UseCase class")
