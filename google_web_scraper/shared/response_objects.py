class ResponseSuccess(object):
    """
    Base class for response success objects
    """

    def __init__(self, value=None):
        """
        :param value: Value to be returned as part of the response success
        """
        self.value = value

    # See https://docs.python.org/2/reference/datamodel.html
    def __nonzero__(self):
        """
        Function to ensure that validation of this object results in True

        :return: True
        """
        return True

    __bool__ = __nonzero__


class ResponseFailure(object):
    """
    Base class for response failure objects. The response failure object supports three different error types:
    - Resource error
    - Parameters error: occurs when invalid parameters where passed
    - System error: occurs when the system failed to execute a function

    """

    RESOURCE_ERROR = 'ResourceError'
    PARAMETERS_ERROR = 'ParametersError'
    SYSTEM_ERROR = 'SystemError'

    def __init__(self, type_, message):
        """
        :param type_: type of response failure (RESOURCE_ERROR, PARAMETERS_ERROR, SYSTEM_ERROR)
        :param message: message related to the failure
        """
        self.type = type_
        self.message = self._format_message(message)

    def _format_message(self, msg):
        """
        Function to format standard exceptions into usable string

        :param msg: exception message to be normalised
        :return: normalised string
        """
        if isinstance(msg, Exception):
            return "{}: {}".format(msg.__class__.__name__, "{}".format(msg))
        return msg

    @property
    def value(self):
        """
        Returns the failure type and related message

        :return: dict with failure type and message
        """
        return {'type': self.type, 'message': self.message}

    def __bool__(self):
        """
        Function to ensure that validation of this object results in False

        :return: False
        """
        return False

    @classmethod
    def build_resource_error(cls, message=None):
        """
        Function to build resource error

        :param message: message related to the failure
        :return: response failure object
        """
        return cls(cls.RESOURCE_ERROR, message)

    @classmethod
    def build_system_error(cls, message=None):
        """
        Function to build system error

        :param message: message related to the failure
        :return: response failure object
        """
        return cls(cls.SYSTEM_ERROR, message)

    @classmethod
    def build_parameters_error(cls, message=None):
        """
        Function to build paremeters error

        :param message: message related to the failure
        :return: response failure object
        """
        return cls(cls.PARAMETERS_ERROR, message)

    @classmethod
    def build_from_invalid_request_object(cls, invalid_request_object):
        """
        Function to build response failure object from an invalid request object.

        :param invalid_request_object: invalid request object
        :return: response failure object
        """
        message = "\n".join(["{}: {}".format(err['parameter'], err['message'])
                             for err in invalid_request_object.errors])
        return cls.build_parameters_error(message)


