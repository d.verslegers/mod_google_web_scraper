===============================
Google Web Scraper
===============================

Tool to search google and scrape the results

* Free software: ISC license
* Documentation: https://google_web_scraper.readthedocs.org.

Features
--------

* TODO

Credits
---------

Tools used in rendering this package:

*  Cookiecutter_
*  `cookiecutter-pypackage`_

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutter-py`: https://webgate-01.afis-mgmt.local/bitbucket/scm/du/cookiecutter-py.git
